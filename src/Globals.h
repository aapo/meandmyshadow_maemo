/****************************************************************************
** Copyright (C) 2011 Luka Horvat <redreaper132 at gmail.com>
** Copyright (C) 2011 Edward Lii <edward_iii at myway.com>
** Copyright (C) 2011 O. Bahri Gordebak <gordebak at gmail.com>
**
**
** This file may be used under the terms of the GNU General Public
** License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef GLOBALS_H
#define GLOBALS_H

#include <SDL/SDL.h>
#include <SDL/SDL.h>
#ifdef BUNDLE_MIXER
#include "../lib/SDL/SDL_mixer.h"
#else
#include "SDL/SDL_mixer.h"
#endif
#include <SDL/SDL_ttf.h>
#include <string>

#ifdef WIN32
//#define DATA_PATH
#else
#include "config.h"
#endif

//Global constants
//The width of the screen.
const int SCREEN_WIDTH=800;
//The height of the screen.

#ifdef MAEMO5
const int SCREEN_HEIGHT=480;
#else
const int SCREEN_HEIGHT=600;
#endif

//The depth of the screen.
const int SCREEN_BPP=32;

//Strin containing the 
const std::string version="V0.2 RC1";

//The height of the current level.
extern int LEVEL_HEIGHT;
//The width of the current level.
extern int LEVEL_WIDTH;

//The target frames per seconds.
const int g_FPS = 40;

#ifdef MAEMO5
/*There are no escape-key on Maemo5/N900*/
/*There are no tab-key on Maemo5/N900*/
#define MEANDMY_ESCAPE_KEY SDLK_q
#define MEANDMY_TAB_KEY SDLK_LSHIFT
#else
#define MEANDMY_ESCAPE_KEY SDLK_ESCAPE
#define MEANDMY_TAB_KEY SDLK_TAB
#endif

//The screen surface, it's used to draw on before it's drawn to the real screen.
extern SDL_Surface* screen;
//SDL_Surface with the same dimensions as screen which can be used for all kinds of (temp) drawing.
extern SDL_Surface* tempSurface;

//The background music.
extern Mix_Music* music;

//The font used for big text like titles.
extern TTF_Font* font;
//The small font used for normal text.
extern TTF_Font* fontSmall;

//Event, used for event handling.
extern SDL_Event event;

//GUI
class GUIObject;
extern GUIObject *GUIObjectRoot;

//The state id of the current state.
extern int stateID;
//Integer containing what the next state will be.
extern int nextState;

//String containing the name of the current level.
extern std::string levelName;

//SDL rectangle used to store the camera.
//x is the x location of the camera.
//y is the y location of the camera.
//w is the width of the camera. (equal to SCREEN_WIDTH)
//h is the height of the camera. (equal to SCREEN_HEIGHT)
extern SDL_Rect camera;

//Enumeration containing the ids of the game states.
enum GameStates
{
	//State null is a special state used to indicate no state.
	//This is used when no next state is defined.
	STATE_NULL,
	
	//This state is for the level editor.
	STATE_LEVEL_EDITOR,
	//This state is for the main menu.
	STATE_MENU,
	//This state is for the actual game.
	STATE_GAME,
	//Special state used when exiting meandmyshadow.
	
	STATE_EXIT,
	//This state is for the help screen.
	
	STATE_HELP,
	//This state is for the level selection.
	STATE_LEVEL_SELECT,
	//This state is for the options screen.
	STATE_OPTIONS,
	//This state is for the addon screen.
	STATE_ADDONS
};

//Enumeration containing the ids of the different block types.
enum GameTileType{
	//The normal solid block.
	TYPE_BLOCK=0,
	//Block representing the start location of the player.
	TYPE_START_PLAYER,
	//Block representing the start location of the shadow.
	TYPE_START_SHADOW,
	//The exit of the level.
	TYPE_EXIT,
	//The shadow block which is only solid for the shadow.
	TYPE_SHADOW_BLOCK,
	//Block that can kill both the player and the shadow.
	TYPE_SPIKES,

	//Special point where the player can save.
	TYPE_CHECKPOINT,
	//Block that will switch the location of the player and the shadow when invoked.
	TYPE_SWAP,
	//Block that will crumble to dust when stepped on it for the third time.
	TYPE_FRAGILE,

	//Normal block that moves along a path.
	TYPE_MOVING_BLOCK,
	//Shadow block that moves along a path.
	TYPE_MOVING_SHADOW_BLOCK,
	//A spike block that moves along a path.
	TYPE_MOVING_SPIKES,

	//Special block which, once entered, moves the player/shadow to a different portal.
	TYPE_PORTAL,
	//A block with a button which can activate or stop moving blocks, converyor belts
	TYPE_BUTTON,
	//A switch which can activate or stop moving blocks, converyor belts
	TYPE_SWITCH,

	//Solid block which works like 
	TYPE_CONVEYOR_BELT,
	TYPE_SHADOW_CONVEYOR_BELT,
	
	//Block that contains a message that can be read.
	TYPE_NOTIFICATION_BLOCK,

	//The (max) number of tiles.
	TYPE_MAX
};

#endif
